Absender:
*Vorname Name, Adresse, PLZ/Ort*

**Einschreiben**

An den  
Nachrichtendienst des Bundes NDB  
Papiermühlestrasse 20  
3003 Bern


Gesuch um Einsicht in die Staatsschutzakten des NDB


Sehr geehrte Damen und Herren

Gestützt auf Art. 63 (Auskunftsrecht) des Nachrichtendienstgesetzes NDG (in Kraft seit dem 1. September 2017) ersuche ich hiermit um Auskunft über sämtliche über mich gespeicherten Daten in den beim NDB geführten Datensystemen.

Herzlichen Dank im voraus für Ihre Bemühungen.

Mit freundlichen Grüssen

*Datum, Unterschrift*

Beilage: Kopie Identitätsausweis
